﻿using Micael.Math.Study.ConsoleUI.Command;
using Micael.Math.Study.ConsoleUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Micael.Math.Study.ConsoleUI
{
    interface IMenu
    {
        void Start();
    }

    public class BLMenu : IMenu
    {
        int spaces = 50;
        private List<Menu> lstMenus = new List<Menu>();

        public BLMenu(List<Menu> menus)
        {
            if (menus == null)
            {
                menus = new List<Menu>();
                menus.Add(new Menu(true) { Nome = "Voltar/Sair", Opcao = "0" });
            }
            lstMenus = menus;
        }

        public  bool Sair(Menu opcaoSelecionada)
        {
            return opcaoSelecionada != null && opcaoSelecionada.IsToClse;
        }

        public void Start()
        {
            while (true)
            {
                PrintMenu();
                try
                {
                    var opcaoSelecionada = OpcaoSelecionada();
                    if (Sair(opcaoSelecionada))
                        break;
                    if (opcaoSelecionada.Command != null)
                    {
                        Console.WriteLine("+".PadRight(spaces, '-'));
                        Console.WriteLine(opcaoSelecionada.Command.GetDescription());
                        Console.WriteLine("+".PadRight(spaces, '-'));
                        Console.WriteLine();
                        opcaoSelecionada.Command.Run(this);
                        Console.WriteLine();
                    }
                    else
                        Console.WriteLine("Opção não implementada");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Opção não implementada, exception");
                    Console.Write("Pretende mostrar a execpção (S/N)? ");
                    if (Console.ReadLine().ToUpper() == "S")
                    {
                        Console.WriteLine(ex.Message);
                        Console.ReadKey();
                    }
                }
            }
        }
        
        private void PrintMenu()
        {

            int number_Sapces = 3;
            if (spaces % 2 != 0)
                spaces++;
            Console.WriteLine("+".PadRight((spaces - 4) / 2, '-') + "Menu" + "+".PadLeft((spaces - 4) / 2, '-'));
            foreach (Menu item in lstMenus)
            {
                Console.WriteLine("|" + item.Opcao.ToString().PadLeft(number_Sapces) + " - " + item.Nome.PadRight(spaces - 2 - number_Sapces - 3) + "|");
            }
            Console.WriteLine("+".PadRight(spaces - 1, '-') + "+");
        }
        public string ReadOpcao()
        {
            return Console.ReadLine();
          
        }
        public Menu FindOpcao(string opcao)
        {
            return lstMenus.FirstOrDefault(o => o.Opcao.ToUpper() == opcao.ToUpper());
        }

        private Menu OpcaoSelecionada()
        {
            Menu result = null;
            while (result == null)
            {
                Console.Write("Indique o numero da opção que pretende executar: ");
                result = FindOpcao(ReadOpcao());
            }
            return result;
        }
    }
}
