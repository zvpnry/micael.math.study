﻿using Micael.Math.Study.ConsoleUI.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Micael.Math.Study.ConsoleUI.Models
{
    public class Menu
    {
        public string Opcao { get; set; }
        public string Nome { get; set; }
        public bool IsToClse { get; private set; }
        public ICommand Command { get; set; }
        public Menu(bool isToClse=false)
        {
            IsToClse = isToClse;
        }

    }
}
