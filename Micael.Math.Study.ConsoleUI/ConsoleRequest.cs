﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Micael.Math.Study.ConsoleUI.Command
{
    public enum EnumOperadores
    {
        soma,
        divisao,
        multiplicacao,
        subtracao
    }

    public static class ConsoleRequest
    {

        public static decimal LerNumero()
        {
            return GetLerNumero(null);
        }

        public static decimal LerNumero(int posicao)
        {

            return GetLerNumero(posicao);
        }

        private static decimal GetLerNumero(int? p)
        {
            decimal result = 0;
            while (true)
            {
                Console.Write("Escreva {0} numero: ", (p == null ? "um" : p.ToString() + "º"));
                if (decimal.TryParse(Console.ReadLine(), out result))
                    break;
            }
            return result;
        }

        public static int GetLerNumeroInt(string message)
        {
            int result = 0;
            while (true)
            {
                Console.Write(message);
                if (int.TryParse(Console.ReadLine(), out result))
                    break;
            }
            return result;
        }


        public static void ShowResultado(object resultado)
        {
            Console.WriteLine("O resultado é: {0}", resultado);
        }

        public static EnumOperadores LerOperadorMath()
        {
            while (true)
            {
                Console.Write("Escreva um Operador: ");

                switch (Console.ReadLine())
                {
                    case "+":
                        return EnumOperadores.soma;
                    case "-":
                        return EnumOperadores.subtracao;
                    case "*":
                        return EnumOperadores.multiplicacao;
                    case "/":
                        return EnumOperadores.divisao;
                }
            }
        }

        public static void EscreveListaDeNumeros(List<int> lista)
        {
            StringBuilder s = new StringBuilder();
            foreach (var item in lista)
            {
                s.Append(item + ",");
            }

            ShowResultado(s.Remove(s.Length - 1, 1));

        }

    }
}
