﻿using Micael.Math.Study.Business;
using Micael.Math.Study.ConsoleUI.Command;
using Micael.Math.Study.ConsoleUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
namespace Micael.Math.Study.ConsoleUI
{
    class Program
    {
        static void Main(string[] args)
        {
            var lstMenus = new List<Menu>();
            lstMenus.Add(new Menu() { Nome = "Soma de números", Opcao = "1", Command = new SumTwoNumbers() });
            lstMenus.Add(new Menu() { Nome = "Quit", Opcao ="q" });
            IMenu menu = new BLMenu(lstMenus);            
            menu.Start();
            /* Menu menu = new Menu();
            Console.WriteLine("Se pretender sair clique em q/Q");

            INumber objNumber = Factory.Number;
            bool isNotToClose = true;
            while (isNotToClose)
            {
                int firstValue = objNumber.getIntNumberDivisibleForMoreThan1AndSameNumber(10, 1, 100);
                List<int> divisibleValues = objNumber.getIntNumbersDivisibleFor(firstValue).Where(x=> x <= 10 && x > 1).ToList();
                int index = objNumber.getIntNumberBetween(0, divisibleValues.Count);
                int secondValue = divisibleValues[index];
                int correctResult = firstValue / secondValue;
                Console.Write("{0}/{1}=", firstValue, secondValue);
                bool notCorrect = true;
                while (notCorrect)
                {
                    string value = Console.ReadLine();
                    if (sair(value))
                    {
                        isNotToClose = false;
                        break;
                    }
                    try
                    {
                        int result = Convert.ToInt32(value);
                        notCorrect = result != correctResult;
                    }
                    catch (Exception ex)
                    {

                    }
                }
            } */
        }

        static bool sair(string value)
        {
            return value.ToUpper() == "Q";
             
        }
    }
}
