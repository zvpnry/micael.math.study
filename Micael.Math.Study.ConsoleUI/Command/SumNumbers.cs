﻿using Micael.Math.Study.Business;
using Micael.Math.Study.ConsoleUI.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Micael.Math.Study.ConsoleUI.Command
{
    public enum NumbersEnum
    {
        POSITIVE = 1,
        NEGATIVE = -1,
        MIX = 0
    }
    public class SumNumbers : ICommand
    {
        NumbersEnum _numbersTypes { get; set; }
        int minValue = 0;
        int maxValue = 0;
        BLMenu _menu;

        public SumNumbers(NumbersEnum numbersTypes)
        {
            _numbersTypes = numbersTypes;
        }

        public string GetDescription()
        {
            switch(_numbersTypes)
            {
                case NumbersEnum.POSITIVE:
                    return "Soma numeros positivos";
                case NumbersEnum.NEGATIVE:
                    return "Soma numeros negativos";
                case NumbersEnum.MIX:
                    return "Soma numeros";
            }
            return "";
        }

        private void Initialize()
        {
            while (true)
            {
                minValue = ConsoleRequest.GetLerNumeroInt("Diga o valor do minimo a usar para calcular: ");
                maxValue = ConsoleRequest.GetLerNumeroInt("Diga o valor do máximo a usar para calcular: ");
                if (minValue > maxValue)
                {
                    Console.WriteLine("O valor minimo é maior que máximo.");
                }
                else if (_numbersTypes == NumbersEnum.POSITIVE && minValue < 0)
                {
                    Console.WriteLine("O valor minimo deve ser igual ou superior a 0.");
                }
                else if (_numbersTypes == NumbersEnum.NEGATIVE && maxValue > 0)
                {
                    Console.WriteLine("O valor máximo deve ser igual ou inferior a 0.");
                }
                else if (_numbersTypes == NumbersEnum.MIX && maxValue < 0 || minValue>0 || maxValue ==minValue)
                {
                    Console.WriteLine("O valor máximo deve ser igual ou superior a 0 e o valor minimo deve ser igual ou inferior a 0 e o minimo não pode ser igual ao máximo.");
                }
                else
                {
                    break;
                }
            }
        }

        public void Run(BLMenu menu)
        {
            _menu = menu;
            Initialize();
            INumber objNumbers = Factory.Number;           
           
            while (true)
            {
                int firstNumber = objNumbers.getIntNumberBetween(minValue, maxValue);
                int secondNumber = objNumbers.getIntNumberBetween(minValue, maxValue);
                Console.Write("{0} + {1} = ",firstNumber,secondNumber);
                while(true)
                {
                    var input = _menu.ReadOpcao();
                    var opcao = _menu.FindOpcao(input);
                    int result = 0;
                    if (isToClose(opcao))
                    {
                        return;
                    }
                    else if(!int.TryParse(input, out result))
                    {
                        Console.WriteLine("Valor inserido não reconhecido.");
                        Console.Write("Nova tentativa: ");
                    }
                    else if ((firstNumber + secondNumber) != result)
                    {
                        Console.Write("Nova tentativa: ");
                    }
                    else
                    {
                        break;
                    }
                }
                
                
            }
        }

        private bool isToClose(Menu opcao)
        {
           return _menu.Sair(opcao);
            
        }
    }
}
