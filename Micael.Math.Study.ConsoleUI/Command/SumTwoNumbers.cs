﻿using Micael.Math.Study.ConsoleUI.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Micael.Math.Study.ConsoleUI.Command
{
    public class SumTwoNumbers : ICommand
    {

        public string GetDescription()
        {
            return "Somar dois números positivos";
        }

        public void Run(BLMenu parentMenu)
        {
            var lstMenus = new List<Menu>();
            lstMenus.Add(new Menu() { Opcao = "a", Nome = "Numeros positivos", Command = new SumNumbers(NumbersEnum.POSITIVE) });
            lstMenus.Add(new Menu() { Opcao = "b", Nome = "Numeros negativos", Command = new SumNumbers(NumbersEnum.NEGATIVE) });
            lstMenus.Add(new Menu() { Opcao = "c", Nome = "Numeros mistos", Command = new SumNumbers(NumbersEnum.MIX) });
            lstMenus.Add(new Menu(true) { Opcao = "Q", Nome = "Pressione para voltar" });
            IMenu menu = new BLMenu(lstMenus);
            menu.Start();
        }
    }
}
