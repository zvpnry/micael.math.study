﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Micael.Math.Study.ConsoleUI.Command
{
    public interface ICommand
    {
        void Run(BLMenu menu);
        string GetDescription();
    }
}
