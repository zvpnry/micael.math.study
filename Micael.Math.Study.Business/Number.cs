﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Micael.Math.Study.Business
{
    internal class Number : INumber
    {
        public int getIntNumber()
        {
            Random r = new Random();
            return r.Next();
        }

        public int getIntNumberDivisibleForMoreThan1AndSameNumber(int maxDivisible, int minValue, int maxValue)
        {
            while (true)
            {
                int value = getIntNumberBetween(minValue, maxValue);
                List<int> divisibles = getIntNumbersDivisibleFor(value).Where(x=> x <maxDivisible).ToList();
                if (divisibles.Count>1)
                {
                    return value;
                }
            }
        }

        public int getIntNumberBetween(int minValue, int maxValue)
        {
            Random r = new Random();
            return r.Next(minValue, maxValue);
        }

        public int getIntNumberLessThan(int value)
        {
            Random r = new Random();
            return r.Next(value);
        }

        public List<int> getIntNumbersDivisibleFor(int value)
        {
            List<int> result = new List<int>();
            for (int i = 1; i <= value; i++)
            {
                if(value%i ==0)
                    result.Add(i);
            }
            return result;
        }
    }
}
