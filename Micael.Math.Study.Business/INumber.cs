﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Micael.Math.Study.Business
{
    public interface INumber
    {
        int getIntNumber();
        int getIntNumberDivisibleForMoreThan1AndSameNumber(int maxDivisible, int minValue, int maxValue);
        List<int> getIntNumbersDivisibleFor(int value);
        int getIntNumberBetween(int minValue, int maxValue);
        int getIntNumberLessThan(int value);

    }
}
