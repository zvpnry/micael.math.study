﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Micael.Math.Study.Business
{
    public static class Factory
    {
        private static Number _number { get; set; }
        public static INumber Number
        {
            get
            {
                if (_number == null)
                    _number = new Number();
                return _number;
            }
        }
    }
}
